<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM27M0U</span>
<span class="globalServcat">_58</span>
<span class="globalServcatName">Vehicle Interior</span>
<span class="globalSection">_049131</span>
<span class="globalSectionName">SUPPLEMENTAL RESTRAINT SYSTEMS</span>
<span class="globalTitle">_0234204</span>
<span class="globalTitleName">AIRBAG SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>SUPPLEMENTAL RESTRAINT SYSTEMS&nbsp;&nbsp;AIRBAG SYSTEM&nbsp;&nbsp;B1835/58&nbsp;&nbsp;Short in Front Passenger Side Curtain Shield Squib Circuit&nbsp;&nbsp;B1836/58&nbsp;&nbsp;Open in Front Passenger Side Curtain Shield Squib Circuit&nbsp;&nbsp;B1837/58&nbsp;&nbsp;Short to GND in Front Passenger Side Curtain Shield Squib Circuit&nbsp;&nbsp;B1838/58&nbsp;&nbsp;Short to B+ in Front Passenger Side Curtain Shield Squib Circuit&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000S47Y_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The curtain shield squib RH circuit consists of the center airbag sensor and the curtain shield airbag RH.
</p>
<p>The circuit instructs the SRS to deploy when the deployment conditions are met.
</p>
<p>These DTCs are recorded when a malfunction is detected in the curtain shield squib RH circuit.
</p>
<table summary="">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC Code
</th>
<th class="alcenter">Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">B1835/58
</td>
<td>When one of the following conditions is met:
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The center airbag sensor receives a line short circuit signal 5 times in the curtain shield squib RH circuit during primary check.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield squib RH malfunction
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor malfunction
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>No. 2 floor wire
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield airbag assembly RH (Curtain shield squib RH)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor assembly
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">B1836/58
</td>
<td>When one of the following conditions is met:
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The center airbag sensor receives an open circuit signal in the curtain shield squib RH circuit for 2 seconds.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield squib RH malfunction
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor malfunction
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>No. 2 floor wire
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield airbag assembly RH (Curtain shield squib RH)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor assembly
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">B1837/58
</td>
<td>When one of the following conditions is met:
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The center airbag sensor receives a short circuit to ground signal in the curtain shield squib RH circuit for 0.5 seconds.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield squib RH malfunction
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor malfunction
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>No. 2 floor wire
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield airbag assembly RH (Curtain shield squib RH)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor assembly
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
<tr>
<td class="alcenter">B1838/58
</td>
<td>When one of the following conditions is met:
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The center airbag sensor receives a short circuit to B+ signal in the curtain shield squib RH circuit for 0.5 seconds.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield squib RH malfunction
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor malfunction
</p>
</div>
</div>
</div>
<br>
</td>
<td><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>No. 2 floor wire
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Curtain shield airbag assembly RH (Curtain shield squib RH)
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Center airbag sensor assembly
</p>
</div>
</div>
</div>
<br>
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div id="RM100000000S47Y_02" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/C146254E45.png" alt="C146254E45" title="C146254E45">
<div class="indicateinfo">
<span class="caption">
<span class="points">0.738,3.136 3.139,3.306</span>
<span class="captionSize">2.401,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">Curtain Shield Squib RH</span>
</span>
<span class="caption">
<span class="points">2.816,1.516 3.190,1.702</span>
<span class="captionSize">0.373,0.186</span>
<span class="fontsize">10</span>
<span class="captionText">ICR+</span>
</span>
<span class="caption">
<span class="points">4.773,1.628 5.174,1.814</span>
<span class="captionSize">0.401,0.186</span>
<span class="fontsize">10</span>
<span class="captionText">ICP+</span>
</span>
<span class="caption">
<span class="points">2.835,2.342 3.190,2.528</span>
<span class="captionSize">0.355,0.186</span>
<span class="fontsize">10</span>
<span class="captionText">ICR-</span>
</span>
<span class="caption">
<span class="points">4.78,2.468 5.172,2.655</span>
<span class="captionSize">0.392,0.186</span>
<span class="fontsize">10</span>
<span class="captionText">ICP-</span>
</span>
<span class="caption">
<span class="points">3.221,1.498 3.363,1.668</span>
<span class="captionSize">0.142,0.170</span>
<span class="fontsize">10</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">3.236,2.348 3.392,2.504</span>
<span class="captionSize">0.156,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">4.584,1.531 4.698,1.694</span>
<span class="captionSize">0.114,0.163</span>
<span class="fontsize">10</span>
<span class="captionText">9</span>
</span>
<span class="caption">
<span class="points">4.516,2.371 4.702,2.534</span>
<span class="captionSize">0.186,0.163</span>
<span class="fontsize">10</span>
<span class="captionText">10</span>
</span>
<span class="caption">
<span class="points">4.758,4.118 4.998,4.302</span>
<span class="captionSize">0.239,0.184</span>
<span class="fontsize">10</span>
<span class="captionText">Q1</span>
</span>
<span class="caption">
<span class="points">4.754,4.268 6.197,4.479</span>
<span class="captionSize">1.443,0.211</span>
<span class="fontsize">10</span>
<span class="captionText">Center Airbag Sensor</span>
</span>
<span class="caption">
<span class="points">0.738,2.975 1.088,3.127</span>
<span class="captionSize">0.350,0.152</span>
<span class="fontsize">10</span>
<span class="captionText">Q3</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000S47Y_03" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the negative (-) battery terminal. Therefore, make sure to read the disconnecting the cable from the negative (-) battery terminal notices before proceeding with work (See page <a class="mlink" href="javascript:void(0)">General&gt;INTRODUCTION&gt;REPAIR INSTRUCTION&gt;PRECAUTION<span class="invisible">201508,999999,_V1,_049090,_0233743,RM100000000S2NJ,</span></a>).
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <a class="mlink" href="javascript:void(0)">General&gt;INTRODUCTION&gt;REPAIR INSTRUCTION&gt;INITIALIZATION<span class="invisible">201508,999999,_V1,_049090,_0233743,RM100000000S2NL,</span></a>).
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Perform the simulation method by selecting "check mode" (signal check) with the Techstream (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;CHECK MODE PROCEDURE<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47J,</span></a>).
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>After selecting "check mode" (signal check), perform the simulation method by wiggling each connector of the airbag system or driving the vehicle on various types of roads (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DIAGNOSIS SYSTEM<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47H,</span></a>).
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM100000000S47Y_04" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM100000000S47Y_04_0001">
<div class="testtitle"><span class="titleText">1.CHECK CURTAIN SHIELD AIRBAG RH (CURTAIN SHIELD SQUIB RH)</span></div>
<div class="content6">
<div class="contentPartition"></div>
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/U152384E02.png" alt="U152384E02" title="U152384E02">
<div class="indicateinfo">
<span class="line">
<span class="points">1.271,0.271 1.427,0.271</span>
<span class="points">1.427,0.271 1.354,1.083</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">2.115,3.792 2.427,3.906</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">1.219,1.281 1.354,1.448</span>
<span class="captionSize">0.135,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">D</span>
</span>
<span class="caption">
<span class="points">1.427,1.281 1.635,1.469</span>
<span class="captionSize">0.208,0.188</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C</span>
</span>
<span class="caption">
<span class="points">2.083,1.344 2.969,1.677</span>
<span class="captionSize">0.885,0.333</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Center Airbag Sensor</span>
</span>
<span class="caption">
<span class="points">0.146,0.188 1.063,0.521</span>
<span class="captionSize">0.917,0.333</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Curtain Shield Squib RH</span>
</span>
<span class="caption">
<span class="points">0.302,2.208 2.635,2.521</span>
<span class="captionSize">2.333,0.313</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of wire harness connector: (to Curtain Shield Squib RH)</span>
</span>
<span class="caption">
<span class="points">0.615,3.365 0.865,3.51</span>
<span class="captionSize">0.25,0.146</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Q3</span>
</span>
<span class="caption">
<span class="points">2.313,3.302 3.146,3.469</span>
<span class="captionSize">0.833,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Connector C</span>
</span>
<span class="caption">
<span class="points">2.49,3.813 2.844,3.979</span>
<span class="captionSize">0.354,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">SST</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Turn the ignition switch off.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the negative (-) terminal cable from the battery, and wait for at least 90 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Disconnect the connector from the curtain shield airbag RH.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Connect the white wire side of SST (resistance: 2.1 Ω) to connector C.
</p>
<dl class="atten2">
<dt class="atten2">CAUTION:</dt>
<dd class="atten2">
<p>Never connect the tester to the curtain shield airbag RH (curtain shield squib RH) for measurement, as this may lead to a serious injury due to airbag deployment.
</p>
</dd>
</dl>
<br>
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Do not forcibly insert SST into the terminals of the connector when connecting SST.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Insert SST straight into the terminals of the connector.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<dl class="sst">
<dt class="sst">SST</dt>
<dd class="sst">09843-18061&nbsp;&nbsp;<br></dd>
</dl>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Connect the negative (-) terminal cable to the battery, and wait for at least 2 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON, and wait for at least 60 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Clear the DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47I,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">h.</div>
<div class="test1Body">
<p>Turn the ignition switch off.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">i.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON, and wait for at least 60 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">j.</div>
<div class="test1Body">
<p>Check for DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47I,</span></a>).
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>DTC B1835, B1836, B1837 or B1838 is not output.
</p>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Codes other than DTC B1835, B1836, B1837 and B1838 may be output at this time, but they are not related to this check.
</p>
</dd>
</dl>
<br>
<table summary="" class="half">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>OK (for Regular Cab and Double Cab)
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td>OK (for CrewMax)
</td>
<td class="alcenter">B
</td>
</tr>
<tr>
<td>NG
</td>
<td class="alcenter">C
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>A
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK CONNECTOR</span></div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CURTAIN SHIELD AIRBAG ASSEMBLY RH</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;CURTAIN SHIELD AIRBAG ASSEMBLY&gt;REMOVAL<span class="invisible">201508,999999,_58,_049131,_0234208,RM100000000S49C,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>C
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CURTAIN SHIELD AIRBAG ASSEMBLY RH</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;CURTAIN SHIELD AIRBAG ASSEMBLY(for CrewMax)&gt;REMOVAL<span class="invisible">201508,999999,_58,_049131,_0234218,RM100000000S4AJ,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000S47Y_04_0002">
<div class="testtitle"><span class="titleText">2.CHECK CONNECTOR</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Turn the ignition switch off.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the negative (-) terminal cable from the battery, and wait for at least 90 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Disconnect SST from connector C.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Check that the No. 2 floor wire connector (on the curtain shield airbag RH side) is not damaged.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>The lock button is not disengaged, or the claw of the lock is not deformed or damaged.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.CHECK NO. 2 FLOOR WIRE (CURTAIN SHIELD SQUIB RH CIRCUIT)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE NO. 2 FLOOR WIRE</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000S47Y_04_0003">
<div class="testtitle"><span class="titleText">3.CHECK NO. 2 FLOOR WIRE (CURTAIN SHIELD SQUIB RH CIRCUIT)</span></div>
<div class="content6">
<div class="contentPartition"></div>
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/U152385E02.png" alt="U152385E02" title="U152385E02">
<div class="indicateinfo">
<span class="line">
<span class="points">0.969,1.01 0.833,1.396</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.417,0.448 1.531,0.448</span>
<span class="points">1.531,0.448 1.531,0.854</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.552,3.656 0.969,4.135</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.667,3.656 2.25,4.177</span>
<span class="whiteedge">true</span>
</span>
<span class="caption">
<span class="points">1.063,1.083 1.25,1.24</span>
<span class="captionSize">0.188,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">D</span>
</span>
<span class="caption">
<span class="points">1.271,1.083 1.458,1.25</span>
<span class="captionSize">0.188,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C</span>
</span>
<span class="caption">
<span class="points">1.656,1.083 1.844,1.25</span>
<span class="captionSize">0.188,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">B</span>
</span>
<span class="caption">
<span class="points">2.021,0.844 2.198,1.021</span>
<span class="captionSize">0.177,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">A</span>
</span>
<span class="caption">
<span class="points">1.948,1.146 2.854,1.49</span>
<span class="captionSize">0.906,0.344</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Center Airbag Sensor</span>
</span>
<span class="caption">
<span class="points">0.177,1.375 1.094,1.698</span>
<span class="captionSize">0.917,0.323</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Curtain Shield Squib RH</span>
</span>
<span class="caption">
<span class="points">0.375,0.354 1.406,0.531</span>
<span class="captionSize">1.031,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">No. 2 Floor Wire</span>
</span>
<span class="caption">
<span class="points">0.208,2.417 2.563,2.729</span>
<span class="captionSize">2.354,0.313</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of wire harness connector: (to Curtain Shield Squib RH)</span>
</span>
<span class="caption">
<span class="points">2.094,3.448 2.948,3.604</span>
<span class="captionSize">0.854,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Connector C</span>
</span>
<span class="caption">
<span class="points">0.646,4.063 1.042,4.24</span>
<span class="captionSize">0.396,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ICR-</span>
</span>
<span class="caption">
<span class="points">2.313,4.094 2.708,4.271</span>
<span class="captionSize">0.396,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ICR+</span>
</span>
<span class="caption">
<span class="points">0.615,3.448 0.917,3.615</span>
<span class="captionSize">0.302,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Q3</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the connectors from the center airbag sensor.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Connect the negative (-) terminal cable to the battery, and wait for at least 2 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Measure the voltage according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard voltage:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:32%">
<col style="width:32%">
<col style="width:34%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Switch Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">Q3-1 (ICR-) - Body ground
</td>
<td class="alcenter" rowspan="2">Ignition switch ON
</td>
<td class="alcenter" rowspan="2">Below 1 V
</td>
</tr>
<tr>
<td class="alcenter">Q3-2 (ICR+) - Body ground
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Turn the ignition switch off.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Disconnect the negative (-) terminal cable from the battery, and wait for at least 90 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">Q3-1 (ICR-) - Q3-2 (ICR+)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">h.</div>
<div class="test1Body">
<p>Release the activation prevention mechanism built into connector B (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DIAGNOSIS SYSTEM<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47H,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">i.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard resistance:</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">Q3-1 (ICR-) - Q3-2 (ICR+)
</td>
<td class="alcenter" rowspan="3">Always
</td>
<td class="alcenter" rowspan="3">1 MΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">Q3-1 (ICR-) - Body ground
</td>
</tr>
<tr>
<td class="alcenter">Q3-2 (ICR+) - Body ground
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.CHECK CENTER AIRBAG SENSOR</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE NO. 2 FLOOR WIRE</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM100000000S47Y_04_0004">
<div class="testtitle"><span class="titleText">4.CHECK CENTER AIRBAG SENSOR</span></div>
<div class="content6">
<div class="contentPartition"></div>
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/C172068E11.png" alt="C172068E11" title="C172068E11">
<div class="indicateinfo">
<span class="line">
<span class="points">1.073,1.156 0.688,1.552</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">0.208,1.542 1.156,1.875</span>
<span class="captionSize">0.948,0.333</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Curtain Shield Squib RH</span>
</span>
<span class="caption">
<span class="points">1.156,1.229 1.281,1.427</span>
<span class="captionSize">0.125,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">D</span>
</span>
<span class="caption">
<span class="points">1.292,1.229 1.438,1.417</span>
<span class="captionSize">0.146,0.188</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C</span>
</span>
<span class="caption">
<span class="points">1.667,1.323 3.031,1.49</span>
<span class="captionSize">1.365,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Center Airbag Sensor</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the connectors to the curtain shield airbag RH and the center airbag sensor.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Connect the negative (-) terminal cable to the battery, and wait for at least 2 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON, and wait for at least 60 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Clear the DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47I,</span></a>).
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Turn the ignition switch off.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON, and wait for at least 60 seconds.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Check for DTCs (See page <a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47I,</span></a>).
</p>
<table summary="" class="half">
<caption>Result</caption>
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td>DTC B1835, B1836, B1837 or B1838 is output (for Column Shift Type)
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td>DTC B1835, B1836, B1837 or B1838 is output (for Floor Shift Type)
</td>
<td class="alcenter">B
</td>
</tr>
<tr>
<td>DTC B1835, B1836, B1837 or B1838 is not output
</td>
<td class="alcenter">C
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Codes other than DTC B1835, B1836, B1837 and B1838 may be output at this time, but they are not related to this check.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>C
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">USE SIMULATION METHOD TO CHECK</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;AIRBAG SYSTEM&gt;DIAGNOSIS SYSTEM<span class="invisible">201508,999999,_58,_049131,_0234204,RM100000000S47H,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>A
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CENTER AIRBAG SENSOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;CENTER AIRBAG SENSOR ASSEMBLY(for Column Shift Type)&gt;REMOVAL<span class="invisible">201508,999999,_58,_049131,_0234217,RM100000000S4AE,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CENTER AIRBAG SENSOR ASSEMBLY</span></div>
<div class="content6">
<p>(See page<a class="mlink" href="javascript:void(0)">Vehicle Interior&gt;SUPPLEMENTAL RESTRAINT SYSTEMS&gt;CENTER AIRBAG SENSOR ASSEMBLY(for Floor Shift Type)&gt;REMOVAL<span class="invisible">201508,999999,_58,_049131,_0234211,RM100000000S49Q,</span></a>)
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
