<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM27M0U</span>
<span class="globalServcat">_V1</span>
<span class="globalServcatName">General</span>
<span class="globalSection">_049090</span>
<span class="globalSectionName">INTRODUCTION</span>
<span class="globalTitle">_0233744</span>
<span class="globalTitleName">HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS</span>
<span class="globalCategory">F</span>
</div>
<h1>INTRODUCTION&nbsp;&nbsp;HOW TO TROUBLESHOOT ECU CONTROLLED SYSTEMS&nbsp;&nbsp;GENERAL INFORMATION&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000S2NN_z0" class="category no00">
<div class="content5">
<div class="list1">
<div class="list1Item">
<div class="list1Head"></div>
<div class="list1Body"><p>A large number of ECU controlled systems are used in the TOYOTA TUNDRA. In general, ECU controlled systems are considered to be very intricate, requiring a high level of technical knowledge to troubleshoot. However, most problem checking procedures only involve inspecting the ECU controlled system's circuits one by one. An adequate understanding of the system and a basic knowledge of electricity is enough to perform effective troubleshooting, accurate diagnoses and necessary repairs.
</p>
</div>
</div>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">TROUBLESHOOTING PROCEDURES
</span></p>
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The troubleshooting procedures consist of diagnosis procedures for when a DTC is stored and diagnosis procedures for when no DTC is stored. The basic idea is explained in the following table.
</p>
<table summary="">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Procedure Type
</th>
<th class="alcenter">Details
</th>
<th class="alcenter">Troubleshooting Method
</th>
</tr>
</thead>
<tbody>
<tr>
<td>DTC Based Diagnosis
</td>
<td>The diagnosis procedure is based on the DTC that is stored.
</td>
<td>The malfunctioning part is identified based on the DTC detection conditions using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the Techstream and inspection of related parts.
</td>
</tr>
<tr>
<td>Symptom Based Diagnosis
<br>
(No DTCs stored)
</td>
<td>The diagnosis procedure is based on problem symptoms.
</td>
<td>The malfunctioning part is identified based on the problem symptoms using a process of elimination.
<br>
The possible trouble areas are eliminated one-by-one by use of the Techstream and inspection of related parts.
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Vehicle systems are complex and use many ECUs that are difficult to inspect independently. Therefore, a process of elimination is used, where components that can be inspected individually are inspected, and if no problems are found in these components, the related ECU is identified as the problem and replaced.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>It is extremely important to ask the customer about the environment and the conditions present when the problem occurred (Customer Problem Analysis). This makes it possible to simulate the conditions and confirm the symptom. If the symptom cannot be confirmed or the DTC does not recur, the malfunctioning part may not be identified using the troubleshooting procedure, and the ECU for the related system may be replaced even though it is not defective. If this happens, the original problem will not be solved.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>In order to prevent endless expansion of troubleshooting procedures, the troubleshooting procedures are written with the assumption that multiple malfunctions do not occur simultaneously for a single problem symptom.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>To identify the malfunctioning part, troubleshooting procedures narrow down the target by separating components, ECUs and wire harnesses during the inspection. If the wire harness is identified as the cause of the problem, it is necessary to inspect not only the connections to components and ECUs but also all of the wire harness connectors between the component and the ECU.
</p>
</div>
</div>
</div>
<br>
</div>
<br>
<dl class="topic">
<dt class="topic">DIAGNOSTIC TESTER</dt>
</dl>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DESCRIPTION
</span></p>
<br>
<p>System data and the Diagnostic Trouble Codes (DTCs) can be read from the Data Link Connector 3 (DLC3) of the vehicle. When the system seems to be malfunctioning, use the Techstream* to check for a malfunction and perform repairs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>*: Techstream is the name for the diagnostic tester in North America.
</p>
</dd>
</dl>
<br>
</div>
<br>
<div class="step1">
<p class="step1"><span class="titleText">DATA LINK CONNECTOR 3 (DLC3)
</span></p>
<br>
<div class="step2">
<div class="step2Item step2HasFigure">
<div class="step2Head">a.</div>
<div class="step2Body">
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/H103092E02.png" alt="H103092E02" title="H103092E02">
<div class="indicateinfo">
<span class="line">
<span class="points">2.125,0.927 2.427,0.594</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.938,0.927 1.823,0.573</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.719,0.917 1.396,0.563</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.542,0.896 1.052,0.583</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">1.656,1.802 1.948,1.479</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">2.365,1.802 2.302,1.49</span>
<span class="whiteedge">true</span>
</span>
<span class="caption">
<span class="points">0.073,0.073 1.292,0.271</span>
<span class="captionSize">1.219,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Front view of DLC3:</span>
</span>
<span class="caption">
<span class="points">0.896,0.406 1.167,0.635</span>
<span class="captionSize">0.271,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">CG</span>
</span>
<span class="caption">
<span class="points">1.24,0.406 1.51,0.635</span>
<span class="captionSize">0.271,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">SG</span>
</span>
<span class="caption">
<span class="points">1.552,0.396 1.958,0.594</span>
<span class="captionSize">0.406,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">CANH</span>
</span>
<span class="caption">
<span class="points">2.313,0.396 2.573,0.594</span>
<span class="captionSize">0.26,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">SIL</span>
</span>
<span class="caption">
<span class="points">1.458,1.802 1.865,2</span>
<span class="captionSize">0.406,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">CANL</span>
</span>
<span class="caption">
<span class="points">2.198,1.813 2.5,2.031</span>
<span class="captionSize">0.302,0.219</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">BAT</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<p>The vehicle's ECU uses the ISO 15765-4 communication protocol. The terminal arrangement of the DLC3 complies with SAE J1962 and matches the ISO 15765-4 format.
</p>
<table summary="">
<colgroup>
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Terminal No. (Symbols)
</th>
<th class="alcenter">Terminal Description
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">7 (SIL) - 5 (SG)
</td>
<td class="alcenter">Bus "+" line
</td>
<td class="alcenter">During transmission
</td>
<td class="alcenter">Pulse generation
</td>
</tr>
<tr>
<td class="alcenter">4 (CG) - Body ground
</td>
<td class="alcenter">Chassis ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">5 (SG) - Body ground
</td>
<td class="alcenter">Signal ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">16 (BAT) - Body ground
</td>
<td class="alcenter">Battery positive
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">11 to 14 V
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 14 (CANL)
</td>
<td class="alcenter">CAN bus line
</td>
<td class="alcenter">Ignition switch OFF*
</td>
<td class="alcenter">54 to 69 Ω
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 4 (CG)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch OFF*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 4 (CG)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch OFF*
</td>
<td class="alcenter">200 Ω or higher
</td>
</tr>
<tr>
<td class="alcenter">6 (CANH) - 16 (BAT)
</td>
<td class="alcenter">HIGH-level CAN bus line
</td>
<td class="alcenter">Ignition switch OFF*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">14 (CANL) - 16 (BAT)
</td>
<td class="alcenter">LOW-level CAN bus line
</td>
<td class="alcenter">Ignition switch OFF*
</td>
<td class="alcenter">6 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
<dl class="atten3">
<dt class="atten3">NOTICE:</dt>
<dd class="atten3">
<p>*: Before measuring the resistance, leave the vehicle as is for at least 1 minute and do not operate the ignition switch, any other switches, or the doors.
</p>
</dd>
</dl>
<br>
<p>If the result is not as specified, the DLC3 may have a malfunction. Repair or replace the harness and connector.
</p>
</div>
</div>
</div>
<br>
<div class="step2">
<div class="step2Item">
<div class="step2Head">b.</div>
<div class="step2Body">
<p>Connect the cable of the Techstream to the DLC3, turn the ignition switch ON and attempt to use the tester. If the display indicates that a communication error has occurred, there is a problem either with the vehicle or with the tester.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is normal when the tester is connected to another vehicle, inspect the DLC3 of the original vehicle.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>If communication is still not possible when the tester is connected to another vehicle, the problem may be in the tester itself. Consult the Service Department listed in the tester's instruction manual.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
